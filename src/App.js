import { Switch, BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import Menu from './components/Menu';
import Article from './views/Article';
import Author from './views/Author';
import Category from './views/Category';
import Home from './views/Home';
import ViewArticle from './components/ViewArticle';
import { useState } from 'react';
import { LangContext } from './util/LangContext';

function App() {

  const [lang, setLang] = useState()

  return (
    <>  
      <LangContext.Provider value={{ lang, setLang }}>
      <BrowserRouter>
        <Menu/>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/article' component={Article}/>
          <Route path='/author' component={Author}/>
          <Route path='/category' component={Category}/>
          <Route path='/view/:id' component={ViewArticle}/>
          <Route path='/update/article/:id' component={Article} />
        </Switch>
      </BrowserRouter>
      </LangContext.Provider>
    </>
  );
}

export default App;
