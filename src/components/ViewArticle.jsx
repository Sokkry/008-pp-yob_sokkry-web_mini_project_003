import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchArticleById } from "../redux/Action/ArticleAction";

export default function ViewArticle() {
  let { id } = useParams();

  const dispatch = useDispatch();
  const state = useSelector((state) => state.ArticleReducer.view);

  useEffect(() => {
    dispatch(fetchArticleById(id));
  }, []);

  
const repo = 'github-namespace/project-name'

  return (
    <Container>
      {state ? (
        <>
          <Row className="mt-3">
            <Col xl="9" >
              <h1 className="my-2">{state.title}</h1> 
              <img width="100%" alt={state.title } className="pb-4 pt-4"
                src={
                  state.image
                    ? state.image
                    : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                }
              />
              <p>{state.description}</p>
            </Col>
            <Col md="3">
              <div>
                <h1 className="mt-5">Comment</h1>
                
              </div>
            </Col>
          </Row>
        </>
      ) : (<h1>Loading...</h1>)}
    </Container>
  );
}
