import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
    en: {
        Home: "Home",
        Category: "Category",
        Article:"Article",
        Author: "Author",
        Category: "Category",
        Language: "Language",
        Khmer: "Khmer",
        English: "English", 
        AddArticle: "Add Article",
        UpdateArticle: "Update Article"
    },
    km: {
        Home: "ទំព័ដើម",
        Category: "ប្រភេទអត្ថបទ",
        Article: "អត្ថបទ",
        Author: "អ្នកនិពន្ធ",
        Category: "ប្រភេទ",
        Language: "ភាសា",
        Khmer: "ខ្មែរ",
        English: "អង់គ្លេស",
        AddArticle: "បន្ថែម អត្ថបទ",
        UpdateArticle: "កែប្រែ អត្ថបទ"
    }
});