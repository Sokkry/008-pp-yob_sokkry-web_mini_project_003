import api from "../../util/api";


//Fetch Aritcle
export const fetchArticle = (page) => async dp =>{
    let response = await api.get(`/articles?page=${page}&size=8`);
    return dp({
        type: "FETCH_ARTICLE",
        payload: response.data
    })
}


//Delete Article
export const deleteArticle = (id) => async dp =>{
    let response = await api.delete('/articles/'+id);
    return dp({
        type: "DELETE_ARTICLE",
        payload: response.data.data
    })
}


//Post Article
export const postArticle = (article) => async dp => {
    let response = await api.post('/articles', article)
    return dp({
        type: "POST_ARTICLE",
        payload: response.data.message
    })
}


// Upload Images
export const uploadImage = (file) => async dp => {
    let formData = new FormData()
    formData.append('image', file)
    let response = await api.post('/images', formData)
    return dp({
        type: "UPLOAD_IMG",
        payload: response.data.url
    })
}


// Fetch Article by id
export const fetchArticleById = (id) => async dp => {
    let response = await api.get('articles/' + id)
    return dp({
        type: "FETCH_ARTICLE_BY_ID",
        payload: response.data.data
    })
}


// Update Article 
export const updateArticleById = (id, newArticle) => async dp => {
    let response = await api.patch('/articles/' + id, newArticle)  
    return dp({
        type: "UPDATE_ARTICLE_BY_ID",
        payload: response.data.message
    })
}