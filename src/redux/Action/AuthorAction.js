import api from "../../util/api"


// Fetch Author
export const fetchAuthor = () => async dp =>{
    let response = await api.get('/author')
    return dp({
        type: "FETCH_AUTHOR",
        payload: response.data.data
    })
}


// Post Author
export const postAuthor = (author) => async dp =>{
    let response = await api.post('/author', author)
    return dp({
        type: "POST_AUTHOR",
        payload: response.data.message
    })
}


// Delete Author
export const deleteAuthor = (id) => async dp => {
    let response = await api.delete('/author/'+id )
    return dp({
        type: "DELETE_AUTHOR",
        payload: response.data.data
    })
}


// Update Author
export const updateAuthor = (id, newAuthor) => async dp => {
    let response = await api.put('/author/'+id, newAuthor )
    return dp({
        type: "UPDATE_AUTHOR",
        payload: response.data.message
    })
}