import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategory } from "../redux/Action/CategoryAction";
import { Container, Form, Table, Button } from "react-bootstrap";
import { strings } from "../localization/localize";

export default function Category() {

  const dispatch = useDispatch();
  const state = useSelector((state) => state.CategoryReducer.categories);

  useEffect(() => {
    dispatch(fetchCategory());
  }, []);
  return (
    <div>
      <Container>
        <h1 className="my-3">{strings.Category}</h1>
        <Form>
          <Form.Group>
            <Form.Label>Category Name</Form.Label>
            <Form.Control
              id="text"
              type="text"
              placeholder="Category Name"
            />
            <br />
            <Button type="submit"> Add </Button>
            <Form.Text className="text-muted"></Form.Text>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {state.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>
                  <Button
                    size="sm"
                    variant="warning">
                    Edit
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger">
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
